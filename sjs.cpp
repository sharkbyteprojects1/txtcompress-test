#include "compress.hpp"
#include <stdio.h>
#include <Config.h>
#include <stdio.h>

extern "C" {
    char* runit(const char* cont){
		puts("SJS - COMPRESSOR\n<c> Sharkbyteprojects");
        std::string out = computeCompressTX(std::string(cont));
        char* e = 
		#ifndef heapalloc
			(char*)alloca(out.length() + 1);
		#else
			new char[out.length() + 1];
		#endif
        strcpy(e, out.c_str());
        return e;
    }
}

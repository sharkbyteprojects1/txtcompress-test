#include "compress.hpp"
#include <queue>
#include <iostream>

std::string computeCompressTX(std::string inp){
    std::vector<std::string> singlewords;
    std::queue<std::string> wordlist;
    {
        std::string stringbuilder{""};
        bool write=false;
        std::string current = " ";
        for(std::string::size_type i = 0; i < inp.size(); ++i) {
            /*inp[i]*/
            bool w3c=false;
            current[0] = inp[i];
            bool sc = current[0]=='!'||current[0]=='?'||current[0]=='.'||current[0]==','||current[0]==';'||current[0]==':'||current[0]=='-';
            if(sc){
                w3c=true;
            }
            else if(current[0] != ' '&&current[0]!='\n'&&current[0]!='\t'&&current[0]!='\r'){
                stringbuilder+=current;
                std::cout << "CSTR: " << stringbuilder << std::endl;
                write=true;
            }else{
                w3c=true;
            }
            if(i >= inp.size() - 1 || w3c){
                if(write){
                    wordlist.push(stringbuilder);
                    stringbuilder = "";
                    write = false;
                }
                if(sc){
                    wordlist.push(current);
                }
            }
        }
    }

    std::string outputs = "", xy = "", oldw="";
    bool firstrun = true;
    while (!wordlist.empty()){
        xy = wordlist.front();
        oldw=xy;

        //MODIFY
        if(xy.size()>3){
            bool firstf = true;
            for(unsigned int x = 0;x < singlewords.size();x++){
                if(xy==singlewords[x]){
                    firstf = false;
                    xy = ("|" + std::to_string(x) + "|");
                    break;
                }
            }
            if(firstf){
                singlewords.push_back(xy);
            }else if(oldw.size() < xy.size()){
                xy=oldw;
            }
        }else{
            singlewords.push_back("!");
        }
        //MODifY END

        std::cout << xy << std::endl;
        if(firstrun){firstrun=false;}else{outputs+=" ";}
        outputs += xy;
        wordlist.pop();
    }
    singlewords.clear(); //CLEANUP
    return outputs;
}
